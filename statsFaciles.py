#!/usr/bin/env python
# coding: utf-8

# # Analyse rapide de résultats
# **objectifs**: réaliser les résumés statistiques et graphiques d'une évaluation, à peu de frais…
# 
# On suppose que les résultats par exercice et total sont bien renseignés dans des champs: voir les fichiers d'exemple `doc{1,2}….ods`.
# 
# ## 1er cas: tous les résultats dans le même onglet: `doc1.ods`
# un champ classe tout de suite disponible: 
# 
# il suffit de charger les données et lancer les résumés.

# In[1]:


import pandas as pd
import altair as alt

# régler la précision ici
pd.set_option('display.float_format', lambda e: format(e, ".2f"))

ALL = pd.read_excel("doc1.ods", sheet_name=0) # onglet n°0


# In[2]:


ALL.describe()


# In[3]:


ALL.columns


# In[4]:


# préparation du chart, les classes en y
ch = alt.Chart(ALL).mark_boxplot(extent='min-max').encode(
    y='classe',
    color=alt.Color('classe').scale(scheme='blues')
)
# affichage en 2 colonnes
im = (ch.encode(x='ex1:Q') | ch.encode(x='ex2:Q')) & (ch.encode(x='ex3:Q') | ch.encode(x='ex4:Q')) &  ch.encode(x='total')
im


# In[5]:


ALL.groupby("classe").describe()


# ## 2em cas: un onglet par classe `doc2_1onglet_par_classe.ods`
# 
# On suppose que le nom des onglets *est* le nom des classes.
# Il faut un peu de travail pour regrouper toutes les données dans un dataFrame:
# 
# - pour chaque onglet, ajouter un champ "classe"
# - assembler une liste des onglets et la concaténer.

# In[6]:


# sheet_name=None: ALL est un dictionnaire des onglets.
df = pd.read_excel("doc2_1onglet_par_classe.ods", sheet_name=None) 
L = []

for e in df:
    df[e].insert(0,"classe", e)
    L.append(df[e])
ALL = pd.concat(L) # concaténation empilée des lignes, par défaut: axe 0


# In[7]:


ALL.columns


# In[8]:


ch = alt.Chart(ALL).mark_boxplot(extent='min-max').encode(
    y='classe',
    color=alt.Color('classe').scale(scheme='blues')
)
# affichage en 2 colonnes
im = (ch.encode(x='ex1:Q') | ch.encode(x='ex2:Q')) & (ch.encode(x='ex3:Q') | ch.encode(x='ex4:Q')) &  ch.encode(x='total')
im


# ## Exports tableur et images
# 
# ### export images
# Les visualisations avec le module `altair` ont un bouton pour sauvegarder les images en svg ou png.
# Si on veut insister pour coder l'export, on utilise le module `vl_convert` en plus:
# 
# D'abord à installer `pip install vl-convert-python`. Le format `svg` est plus économe en taille de sortie (`png` est généré à partir d'un `svg` intermédiaire)
# 

# In[9]:


import vl_convert as vlc
data_png = vlc.vegalite_to_png(vl_spec=im.to_json(), scale=2)
with open(f"diagrammes_boite.png", "wb") as f:
    f.write(data_png)
data_svg = vlc.vegalite_to_svg(vl_spec=im.to_json()) # pas de scale en svg
with open(f"diagrammes_boite.svg", "w") as f: #svg format textuel, pas de b
    f.write(data_svg)


# ### export tableaux
# 
# On invoque un export `pandas` vers un objet `ExcelWriter`:

# In[11]:


with pd.ExcelWriter("statsDevoir.xlsx") as writer:
    # résumé général et par classe
    ALL.describe().to_excel(writer,
                            float_format="%.2f",
                            sheet_name="global")
    ALL.groupby("classe").describe().to_excel(writer,
                                              float_format="%.2f",
                                              sheet_name="par_classe")

